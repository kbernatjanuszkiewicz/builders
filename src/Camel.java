public class Camel {
    private int id, age, weigth, height;
    private String name;
    private Gender gender;
    private Color color;
    private boolean achieved;

    public static NeedID builder() {
        return new Builder();
    }

    public static class Builder implements NeedID, NeedName, NeedAge, NeedGender, CanBeBuild {
        private int id, age, weight, height;
        private String name;
        private Gender gender;
        private Color color;
        private boolean achieved = false;

        @Override
        public Builder id(int id) {
            this.id = id;
            return this;
        }

        @Override
        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @Override
        public Builder age(int age) {
            this.age = age;
            return this;
        }

        @Override
        public Builder gender(Gender gender) {
            this.gender = gender;
            return this;
        }

        @Override
        public CanBeBuild needWeight(int weight) {
            this.weight = weight;
            return this;
        }

        @Override
        public CanBeBuild needHeight(int height) {
            this.height = height;
            return this;
        }

        @Override
        public CanBeBuild needColor(Color color) {
            this.color = color;
            return this;
        }

        @Override
        public Builder achieved() {
            this.achieved = true;
            return this;
        }

        @Override
        public Builder and() {
            return this;
        }


        public Camel build() {
            Camel camel = new Camel();
            camel.id = this.id;
            camel.name = this.name;
            camel.age = this.age;
            camel.gender = this.gender;
            camel.color = this.color;
            camel.height = this.height;
            camel.weigth = this.weight;
            camel.achieved = this.achieved;

            return camel;
        }
    }

    public interface NeedID {
        public NeedName id(int id);
    }

    public interface NeedName {
        public NeedAge name(String name);
    }

    public interface NeedAge {
        public NeedGender age(int age);
    }

    public interface NeedGender {
        public Builder gender(Gender gender);
        CanBeBuild and();
    }

    public interface CanBeBuild {
        CanBeBuild needWeight (int weight);
        CanBeBuild needHeight (int height);
        CanBeBuild needColor (Color color);

        CanBeBuild achieved();
        Camel build();
    }
//
//    public interface NeedWeight {
//        public NeedHeight height(int weight);
//    }
//
//    public interface NeedHeight {
//        public NeedColor color(int height);
//    }
//
//    public interface NeedColor {
//
//    }



}
