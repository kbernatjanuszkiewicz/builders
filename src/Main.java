public class Main {

    public static void main(String[] args) {

        Camel camel = Camel.builder()
                .id(001)
                .name("Antek")
                .age(10)
                .gender(Gender.MALE)
                .build();

        Camel camel1 = Camel.builder()
                .id(002)
                .name("Maniek")
                .age(8)
                .gender(Gender.MALE)
                .and()
                .needHeight(100)
                .needWeight(400)
                .needColor(Color.LIGHT_YELLOW)
                .build();
    }
}
